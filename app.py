from PIL import Image
from PIL import ImageOps
from PIL import ImageEnhance

class PictureEditor:
    input = None
    output = None

    def __init__(self, input_file):
        self.input = Image.open("in_pictures/%s" % input_file)

    def show(self, title=None):
        self.output.show(title)

    def save(self, output_file):
        self.output.save("out_pictures/%s" % output_file)
        self.output = None

    def crop(self, left=200, up=200, right=200, bottom=200):
        """
        Crop image from each side
        default: 200px
        """
        image = self.output or self.input
        self.output = ImageOps.crop(image, (left, up, right, bottom))

        return self

    def gray_filter(self):
        """
        Black and white image ( gray filter )
        """
        image = self.output or self.input
        self.output = image.convert("L")

        return self

    def brightness(self, value):
        """
        Set brightness for image
        value: 1 = 100%
        increase: 1.5 = 150%
        decrease: 0.8 = 80%
        """
        image = self.output or self.input
        self.output = ImageEnhance.Brightness(image).enhance(value)

        return self

    def rotate(self, degrees=90):
        """
        Rotate image
        default: 90 degrees
        """
        image = self.output or self.input
        self.output = image.rotate(degrees, expand=True)

        return self

    def mirror(self, left_to_right=True):
        """
        Mirror image
        Default: left to right
        """
        image = self.output or self.input
        option = Image.FLIP_LEFT_RIGHT if left_to_right else Image.FLIP_TOP_BOTTOM
        self.output = image.transpose(option)

        return self


    def transition(self, file, x_start):
        """
        Fade between two images
        :param file: file name in in_pictures directory
        :param x_start: where to start transition ( horizontally )
        """
        image = self.output or self.input
        image2 = Image.open("in_pictures/%s" % file)
        width, height = image.size
        output = Image.new("RGB", (width, height), (255,255,255))

        i = 0.01
        for x in range(width):
            ratio1 = i
            ratio2 = 1.00 - ratio1
            for y in range(height):
                rgb1 = image.getpixel((x, y))
                rgb2 = image2.getpixel((x, y))

                red = int(ratio1*rgb1[0] + ratio2*rgb2[0])
                green = int(ratio1*rgb1[1] + ratio2*rgb2[1])
                blue = int(ratio1*rgb1[2] + ratio2*rgb2[2])

                output.putpixel((x, y), (red, green, blue))
            if i < 0.99 and x > x_start:
                i += 0.01
        self.output = output

        return self


editor = PictureEditor("image1.jpg")
editor.transition("image2.jpg", 500).save("omg.jpg")
# editor.mirror().save("omg.jpg")








